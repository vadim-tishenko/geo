/**
 * Created by vadim.tishenko
 * on 05.10.2018 23:54.
 */
//---------------------------------------------------------------------
//
// Copyright (c) 2011, Karl Magnusson, Softhouse Consulting AB
//
// This code may be used freely, in part or whole, for any
// non-commercial products/projects. If the code, in part or whole, is
// used in a commercial product the developer(s) integrating said code
// must sing a song at the next Softhouse monthly meeting. This is to
// take place after the meeting has been started but before any
// alcoholic beverages are distributed.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

//---------------------------------------------------------------------
//
// This is probably an implementation of the Encoded Polyline
// Algorithm Format algorithm detailed at:
//
//   http://code.google.com/apis/maps/documentation/utilities/polylinealgorithm.html

public class Googli {

    StringBuilder output = new StringBuilder();
    double lastLatitude  = 0;
    double lastLongitude = 0;

    //--------------------------------------------------
    //
    //  Instance methods
    //
    public Googli(double latitude, double longitude) {
        addRelative(latitude, longitude);
    }

    public void addAbsolute(double latitude, double longitude) {
        addRelative(latitude - lastLatitude, longitude - lastLongitude);
    }

    public void addRelative(double latitude, double longitude) {
        lastLatitude  += latitude;
        lastLongitude += longitude;
        output.append(convert(latitude));
        output.append(convert(longitude));
    }

    public String get() {
        return output.toString();
    }

    //--------------------------------------------------
    //
    //  Helper methods
    //
    static int binary(int value) {
        if (value < 0) {
            value = (~(-value)) + 1;
            value = value << 1;
            value = ~value;
        }
        else
            value = value << 1;

        return value;
    }

    static int[] fiveBits(int value) {
        int highest  = Integer.SIZE - Integer.numberOfLeadingZeros(value);
        int[] output = new int[(int)((double)highest / 5 + 0.99)];
        int mask     = Integer.parseInt("11111", 2);

        for (int i = 0; i < output.length; ++i) {
            if (i == output.length - 1)
                output[i] = (value & mask) + 63;
            else
                output[i] = ((value & mask) | 0x20) + 63;

            value = value >> 5;
        }

        return output;
    }

    static String ascii(int[] values) {
        StringBuilder sb = new StringBuilder(values.length);
        for (int i : values)
            sb.append((char)i);

        return sb.toString();
    }

    public static String convert(double value) {
        int integer = (int)Math.round(value * 100000.0);
        return ascii(fiveBits(binary(integer)));
    }

    //--------------------------------------------------
    //
    //  Tests
    //
    static void test(double input, String output) throws Exception {
        if (!convert(input).equals(output))
            throw new Exception(String.format("Failed. Wanted %s but got %s for %f",
                    output, convert(input), input));
    }

    static void testObject() throws Exception {
        Googli gRelative = new Googli(38.5, -120.2);
        Googli gAbsolute = new Googli(38.5, -120.2);

        gRelative.addRelative(2.2, -0.75);
        gAbsolute.addAbsolute(40.7, -120.95);

        gRelative.addRelative(2.552, -5.503);
        gAbsolute.addAbsolute(43.252, -126.453);

        String result = "_p~iF~ps|U_ulLnnqC_mqNvxq`@";

        if (!result.equals(gRelative.get()))
            throw new Exception(String.format("Relative adding failed. Wanted %s but got %s",
                    result, gRelative.get()));

        if (!result.equals(gAbsolute.get()))
            throw new Exception(String.format("Absolute adding failed. Wanted %s but got %s",
                    result, gAbsolute.get()));
    }

    public static void main(String [] args) throws Exception {
        test(10.0, "_c`|@");
        test(50.0, "_sdpH");
        test(179.0, "_}oca@");

        test(-10.0, "~b`|@");
        test(-50.0, "~rdpH");
        test(-179.0, "~|oca@");

        test(38.5,  "_p~iF");
        test(2.2,   "_ulL");
        test(2.552, "_mqN");

        test(-179.9832104, "`~oia@");
        test(-120.1,       "~_`|U");
        test(-120.2,       "~ps|U");
        test(-120.3,       "~ag}U");
        test(-0.75,        "nnqC");
        test(-5.503,       "vxq`@");

        testObject();

        System.out.println("Test ok!");
    }
}
