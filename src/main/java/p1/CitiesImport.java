package p1;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by vadim.tishenko
 * on 04.10.2018 21:56.
 */
public class CitiesImport {
    public static void main(String[] args) throws IOException, URISyntaxException {
        List<City> res = new CitiesImport().getCities();
        for (City re : res) {
            System.out.println(re);
        }
    }
    public List<City> getCities()  {
        URI a = null;
        try {
            a = CitiesImport.class.getResource("/data/cities.csv").toURI();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Path b = Paths.get(a);
        Stream<String> aa = null;
        try {
            aa = Files.lines(b);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<City> res = aa.skip(1).map(c -> {
            final String[] ar = c.split(",");
            final String x = ar[5] + '.' + ar[6];
            String utc = ar[16];
            String lan = ar[17];
            String lon = ar[18];
            return new City(x, utc, lan, lon);

        }).collect(Collectors.toList());
        return res;
    }
}
