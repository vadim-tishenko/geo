package p1;

import java.util.List;

/**
 * Created by vadim.tishenko
 * on 04.10.2018 22:38.
 */
public class CityService {
    private List<City> cities;

    public CityService(List<City> cities) {
        this.cities = cities;
    }

    public List<City>getCities(){
        return cities;
    }
}
