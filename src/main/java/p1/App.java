package p1;

import io.javalin.Javalin;

import java.time.Instant;
import java.time.LocalDateTime;


/**
 * Created by vadim.tishenko
 * on 03.10.2018 19:14.
 */


public class App {
    public static void main(String[] args) {
        CityService cityService = new CityService(new CitiesImport().getCities());
        Javalin app = Javalin.create().start(7000);
        app.get("/", ctx -> ctx.result("Hello World"));
        app.get("/city", ctx -> {
            ctx.json(cityService.getCities());
            ctx.header("aaa", LocalDateTime.now().toString());
            ctx.header("bbb", Instant.now().toString());

        });
    }
}
