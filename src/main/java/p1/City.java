package p1;

/**
 * Created by vadim.tishenko
 * on 04.10.2018 22:38.
 */
public class City{
    private String name;
    private String utc;
    private String lan;
    private String lon;

    public City(String name, String utc, String lan, String lon) {
        this.name = name;
        this.utc = utc;
        this.lan = lan;
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public String getUtc() {
        return utc;
    }

    public String getLan() {
        return lan;
    }

    public String getLon() {
        return lon;
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", utc='" + utc + '\'' +
                ", lan='" + lan + '\'' +
                ", lon='" + lon + '\'' +
                '}';
    }
}
