package geocoder;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vadim.tishenko
 * on 05.10.2018 21:58.
 */
public class Sputnik {
    private static String baseUrl = "http://routes.maps.sputnik.ru/osrm/router/viaroute";
    /*
    http://routes.maps.sputnik.ru/osrm/router/viaroute?loc=52.503033,13.420526&loc=52.516582,13.429290&instructions=true
     */
//http://{baseUrl}?loc={lat,lon}&loc={lat,lon}<&loc={lat,lon} ...>

    String mkUrl(List<Loc> locations) {
        StringBuilder sb = new StringBuilder(baseUrl);
        sb.append('?');
        for (Loc l : locations) {
            sb.append("loc=").append(l.lat).append(',').append(l.lon).append('&');
        }
        sb.append("instructions=true");
        return sb.toString();
    }

    void dod(String url) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(url);

        try (CloseableHttpResponse response = httpclient.execute(httpget)) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                long len = entity.getContentLength();
//                if (len != -1 && len < 20480) {
                    System.out.println(EntityUtils.toString(entity));
//                } else {
                    // Stream content out
//                }
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Loc msk = Loc.of("37.485199", "55.874784");
        Loc ptr = Loc.of("30.335016", "59.937604");
        List<Loc> locs=new ArrayList<>();
        locs.add(msk);
        locs.add(ptr);
        Sputnik sp = new Sputnik();
//        sp.dod(sp.mkUrl(locs));
        locs.clear();
        locs.add(ptr);
        locs.add(msk);
        sp.dod(sp.mkUrl(locs));



    }
}

class Loc {
    String lon;
    String lat;

    public Loc(String lon, String lat) {
        this.lon = lon;
        this.lat = lat;
    }

    static Loc of(String lon, String lat) {
        return new Loc(lon, lat);
    }
}
